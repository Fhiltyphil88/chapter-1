const { createStatementData } = require('./../../src/createStatementData.js')

describe('Testing the createStatementData file', () => {
  const plays = {
    hamlet: {
      name: 'Hamlet',
      type: 'tragedy',
    },
    'as-like': {
      name: 'As You Like It',
      type: 'comedy',
    },
    'as-like-2': {
      name: 'As You Like It 2',
      type: 'comedy',
    },
    othello: {
      name: 'Othello',
      type: 'tragedy',
    },
  }
  const invoices = [
    {
      customer: 'BigCo',
      performances: [
        {
          playID: 'hamlet',
          audience: 55,
        },
        {
          playID: 'as-like',
          audience: 12,
        },
        {
          playID: 'as-like-2',
          audience: 21,
        },
        {
          playID: 'othello',
          audience: 20,
        },
      ],
    },
  ]
  it('Should call the createStatementData function', () => {
    const expected = {customer: 'BigCo', totalAmount: 185400, totalVolumeCredits: 31}
    const expectedPerformances = [{
      'amount': 65000,
      'audience': 55,
      'play':  {
        'name': 'Hamlet',
        'type': 'tragedy',
      },
      'playID': 'hamlet',
      'volumeCredits': 25,
    }]

    const result = createStatementData(invoices[0], plays)

    expect(result).toMatchObject(expected)
    expect(result.performances).toEqual(expect.arrayContaining(expectedPerformances))
  })
})
