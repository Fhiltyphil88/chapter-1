const {renderHtml, renderPlainText} = require('./src/statement')
const { createStatementData } = require('./src/createStatementData')

function statement(invoice, plays) {
  return renderPlainText(createStatementData(invoice, plays))
}

function htmlStatement(invoice, plays) {
  return renderHtml(createStatementData(invoice, plays))
}

module.exports = {
  statement,
  htmlStatement
}